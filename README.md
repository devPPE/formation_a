# Initiation MQTT, NodeRed, Arduino

## Introduction aux technologie et outils

### Environnement de test

L\'environnement de test se présentera sous la forme d\'un mini projet
permettant le pilotage d\'un bandeau led et l\'écoute de l\'action sur
un bouton brancher sur une carte ESP32 compatible Arduino avec des
échanges MQTT pour communiquer avec un serveur NodeRed 

![](/Formation_Kurioscape_fichiers/image002.jpg)

Pour cela nous allons présenter les éléments suivants :

1.  Le serveur NodeRed

2.  La carte ESP32

3.  L\'environnement Arduino

4.  Le protocole MQTT

Une fois cela présenter, nous passerons à l\'installation complète de
l\'environnement sont utilisation pour arriver au résultat attendu.

Les outils tourneront sur un Raspberry. Celui-ci permettra d\'héberger
le serveur NodeRed mais aussi les service MQTT pour y connecter tous nos
modules compatibles.

### Le serveur NodeRed

NodeRed est un service permettant de gérer un serveur avec une interface
simplifier et \"sans\" code. Il permet grâce à de simple nœuds et
connexion de programmer des actions.

#### Interface
![](/Formation_Kurioscape_fichiers/image004.gif)

L'interface de Node-RED se compose de 4 parties, qui sont :

1.  La liste des nœuds disponible\
    Il s'agit de la liste des nœuds disponible avec NodeRed pour
    l'édition de programme. Il est possible d\'en rajouter en installant
    des plugins pour gérer différente tache (Accès à une base de
    données, Utilisation de map, Utilisation de service en ligne
    \"alerte SMS\", \"alerte mail\" \...)

2.  Les flows\
    L\'éditeur de flow permet grâce au nœud et a des liens entre eux de
    programmer les actions voulus.\
    Il est possible de créer plusieurs flows pour séparer les
    différentes logiques (exemple : flow pour la gestion des scores,
    flow pour la gestion des salles \...)

3.  Fenêtre d\'information\
    Cette fenêtre regroupe plusieurs onglets permettant d\'obtenir des
    informations sur diffèrent aspects de l\'environnement NodeRed

    a.  Debug

    b.  Context

    c.  Documentation

#### Nœuds de base

1.  Common\
    Nœuds communes, permettant des opérations simples sans traitements.

    a.  Permet d'afficher un message de debug.

> ![](/Formation_Kurioscape_fichiers/image006.jpg)

b.  Permet de lancer une action depuis Node Red pour activer les nœuds
    avec une information d\'entré spécifique

>![](/Formation_Kurioscape_fichiers/image008.jpg)

2.  Function\
    Nœuds permettant d'agir sur les messages, de modifier leur contenu,
    de leur soumettre des traitements, et d'influer légèrement sur la
    façon dont ils sont délivrés

    a.  Permet de créer une fonction en JavaScript. Utile pour traiter
        un message reçu pour le rendre utilisable par une fonction js 

> ![](/Formation_Kurioscape_fichiers/image010.jpg)

b.  Permet d'ajouter un délai d'exécution entre deux nœuds.

> ![](/Formation_Kurioscape_fichiers/image012.jpg)

3.  MQTT\
    Nœuds permettant la gestion des flux MQTT

    a.  Permet de recevoir des message MQTT et de les utiliser dans des
        nœuds par la suite

> ![](/Formation_Kurioscape_fichiers/image014.jpg)

b.  Permet l\'envois de messages MQTT depuis un flux NodeRed

> ![](/Formation_Kurioscape_fichiers/image016.jpg)

#### Edition de flows

Pour créer un programme il faut :

![](/Formation_Kurioscape_fichiers/image018.jpg)

1.  Ajouter des nœuds

2.  Les paramétrer

3.  Les associer par des liaisons

> Les nœuds associé échange des données grâce à une variable « msg ».
> Celle-ci est constituée par défaut d\'un « payload » et d\'un
> « topic » qui peuvent être définis et récupérer par les nœuds ou les
> utilisateurs.

### La carte ESP32

La carte ESP32 est une carte programmable avec la librairie Arduino
permettant d\'exécuter du code. Elle est capable de communiquer Wifi,
Bluetooth, Ethernet selon les modèles. Elles possèdent des pins pouvant
être configurer en entrer ou en sortie

-   En **entré**, elles permettent d\'écouter des messages venant de
    différentes interfaces tel que des capteurs de température, de
    simple bouton ou encore la communication avec un lecteur RFID \...

-   En **sortie**, elles permettent de piloter différent élément allant
    de la simple Led au contrôle de module externe comme l\'ouverture de
    porte, L\'envois de SMS à partir d\'un module GSM etc.

Elle a l\'avantage d\'être compatible avec un grand nombre de protocole,
de consommer très peu et d\'être très peu cher.

![](/Formation_Kurioscape_fichiers/image020.jpg)

#### L\'environnement Arduino

L\'Arduino, combiné à la carte ESP32, offre une plateforme flexible pour
les projets de développement embarqué et IoT (Internet des Objets).

Le démarrage avec l\'ESP32 sous l\'environnement Arduino est simple.

Après l\'installation de l\'IDE Arduino et l\'ajout de l\'ESP32 à la
liste des cartes via le gestionnaire de cartes, Il est possible de
programmer la carte avec toutes les librairies accessibles dans Arduino

Il faut ensuite écrire le code et le charger sur la carte Arduino.

[Exemple de code]
```
void setup() {  

 // Configure la broche 13 en sortie  

 pinMode(13, OUTPUT);  

}

void loop() {

 digitalWrite(13, HIGH);   // Allume la LED  

 delay(1000);               // Attends une seconde (1000 millisecondes)
 

 digitalWrite(13, LOW);    // Éteint la LED  

 delay(1000);               // Attends encore une seconde  

}
```

setup() : Cette fonction est appelée une seule fois lorsque le programme
commence. Elle est utilisée pour initialiser les paramètres, comme la
configuration des broches en entrée ou en sortie. Dans notre exemple,
pinMode(13, OUTPUT); configure la broche numérique 13 comme une sortie
cela est nécessaire pour contrôler la LED.

loop() : Après setup(), la fonction loop() est appelée de manière
répétitive aussi longtemps que l\'appareil est sous tension. Cela permet
au programme de changer d\'état ou de réagir à des événements en
continu. Dans l\'exemple, nous utilisons loop() pour faire clignoter une
LED :

digitalWrite(13, HIGH); envoie une tension haute (5V ou 3.3V selon la
carte) sur la broche 13, allumant ainsi la LED.

delay(1000); fait pause le programme pendant 1000 millisecondes (1
seconde), laissant la LED allumée pendant ce temps.

digitalWrite(13, LOW); envoie ensuite une tension basse (0V) sur la
broche 13, éteignant la LED.

Un autre delay(1000); fait de nouveau pause le programme pendant 1
seconde, avec la LED éteinte cette fois.

La fonction loop() recommence ensuite, créant un effet de clignotement.

#### Les librairies importantes

**MQTT , Wifi et Leds**\
Pour utiliser le protocole MQTT et les Leds sur un Arduino ou un ESP32,
Nous aurons besoin des librairies suivantes :

-   PubSubClient

-   Wifi

-   NeoPixelBus

-   Adafruit_NeoPixel

Nous les installerons à partir du gestionnaire de bibliothèque présent
dans l'éditeur Arduino.

[Prérequis :]{.underline}

Voici un exemple simple qui montre comment connecter un ESP32 à un
broker MQTT et publier/envoyer des messages sur un topic.
```
#include \<WiFi.h\>

#include \<PubSubClient.h\>

// Paramètres WiFi

const char\* ssid = \"yourSSID\";

const char\* password = \"yourPASSWORD\";

// Paramètres du serveur MQTT

const char\* mqtt_server = \"BrokerAdress\";

const int mqtt_port = 1883;

const char\* mqtt_user = \"yourMQTTusername\"; // Laissez vide si non
requis

const char\* mqtt_password = \"yourMQTTpassword\"; // Laissez vide si
non requis

WiFiClient espClient;

PubSubClient client(espClient);

void setup_wifi() {

delay(10);

// Connexion au réseau WiFi

WiFi.begin(ssid, password);

while (WiFi.status() != WL_CONNECTED) {

delay(500);

}

// WiFi connecté

}

void callback(char\* topic, byte\* payload, unsigned int length) {

// Gérer les messages entrants

}

void reconnect() {

// Boucle jusqu\'à ce que nous soyons reconnectés

while (!client.connected()) {

if (client.connect(\"ESP32Client\", mqtt_user, mqtt_password)) {

// Abonnement

client.subscribe(\"yourTopic\");

} else {

delay(5000);

}

}

}

void setup() {

setup_wifi();

client.setServer(mqtt_server, mqtt_port);

client.setCallback(callback);

}

void loop() {

if (!client.connected()) {

reconnect();

}

client.loop();

// Publie un message toutes les 5 secondes

client.publish(\"yourTopic\", \"hello world\");

delay(5000);

}
```
setup_wifi() : Cette fonction se connecte à votre réseau Wi-Fi.

callback() : Fonction appelée chaque fois qu\'un message est reçu sur un
des topics souscrits. Ici, vous pouvez ajouter le code pour traiter les
messages.

reconnect() : Tente de se reconnecter au broker MQTT et de souscrire à
un topic.

setup() : Initialise la connexion Wi-Fi et configure le client MQTT avec
le serveur et le port MQTT, et définit la fonction de callback pour les
messages entrants.

loop() : S\'assure que le client est toujours connecté au broker MQTT et
publie un message sur un topic spécifique toutes les 5 secondes.

### Le protocol MQTT

#### Présentation

MQTT est un protocole d'envoi de données basé sur tcp/ip.

![](/Formation_Kurioscape_fichiers/image022.jpg)

Il se divise en 3 types d'entités :

-   Les publishers, qui envoient des messages

-   Les listeners ou subscribers qui écoutent pour recevoir des messages

-   Le broker, le serveur qui fait le lien entre les 2.

Les publishers et listeners communiquent grâce aux topics auquels ils
sont « abonnés » (subscribed).

#### Utilisation avec NodeRed

![](/Formation_Kurioscape_fichiers/image026.gif)

-   Le **MQTT OUT** s'abonne à ces topics et y envoie les messages.

-   Le **MQTT IN** reçoit les messages de tous les topics, en étant
    abonnée au topic '#', qui représente tous les topics possibles.

Les nœuds **MQTT OUT** et **MQTT IN**  in ont les mêmes propriétés, qui
sont :

-   Server : Le serveur sur lequel se trouve le broker. Le port par
    défaut est 1883.

-   Topic : Le Topic auquel s'abonner.

-   QoS : Quality of Service, détermine la façon dont sont traités les
    messages

-   ![](/Formation_Kurioscape_fichiers/image028.jpg)

Le nœud MQTT OUT possède également la « property retain », qui détermine
si le broker doit retenir le message même si aucun listener n'est abonné
au bon topic.

### Le Projet

Pour le projet toute les source sont trouvable sur un dépôt en ligne
: <https://gitlab.com/devPPE/formation_a>

#### Installation RPI

##### Préparation de la carte SD

-   Télécharger l\'utilitaire raspberry
    sur <https://downloads.raspberrypi.org/imager/imager_latest.exe>

-   Installer et lancer l\'application

    -   Sélectionner le model du raspberry

    -   Sélectionner l\'OS RASPBERRY PI OS

    -   Sélectionner le stockage préalablement charger (carte micro SD)

-   Faire suivant

-   Il demande si l\'on veut appliquer des configuration custom. Faire
    modifier les réglage

    -   Dans Général

        -   Définir un nom d\'hôte unique pour pouvoir si connecter
            facilement plus tard en SSH

        -   Définir un nom d\'utilisateur et un mot de passe

        -   Définir le SSID et mot de passe du wifi

    -   Dans Services

        -   Activer le SSH

-   Enregistrer

##### Connection et préparation du Raspberry

Brancher le Raspberry avec la carte flasher et connecter vous a celui-ci
en ssh

-   Ouvrir un terminal

ssh \[nomUser\]@\[hostname\]

-   Définissez l\'utilisateur root et son mot de passse

sudo passwd root

-   Passer en root

su -

-   installer ensuite node-red

> sudo apt install build-essential git curl\
> bash \<(curl
> -sL <https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered>)
> sudo systemctl enable nodered.service

-   Fixer l\'adresse IP

> sudo nano /etc/dhcpcd.conf\
> interface eth0\
> static ip_address=192.168.1.100/24\
> static routers=192.168.1.1\
> static domain_name_servers=192.168.1.1 8.8.8.8\
> sudo reboot

-   installer le broker MQTT mosquitto

> sudo apt-get install mosquitto sudo apt-get -y install
> mosquitto-clients\
> sudo nano /etc/mosquitto/mosquitto.conf\
> sudo systemctl enable mosquitto.service

-   ajouter a la fin du fichier (/etc/mosquitto/mosquitto.conf)

> port 1883\
> listener 9001\
> protocol websockets

Le Raspberry est maintenant prêt à être utilisé

#### Préparation NodeRed

-   Connecter vous au même réseau que le Raspberry

-   Lancer un navigateur sur le
    lien [192.168.1.100:1880](https://file+.vscode-resource.vscode-cdn.net/c%3A/Users/User/Desktop/formation_a/192.168.1.100:1880)

-   Récupérer le fichier « NR_Formation_kurio.json »:\
    <https://gitlab.com/devPPE/formation_a/-/raw/main/Node_Red/NR_Formation_kurio.json?ref_type=heads&inline=false>

-   Dans NodeRed, aller dans les menus en haut à droite de la fenêtre,
    choisir l'option importer et utilisez le fichier téléchargé.

-   

#### Préparation ESP32

Le code source du projet est trouvable
sur [https://gitlab.com/devPPE/formation_a/-/tree/main/Arduino/AnimationLed_Button](https://gitlab.com/devPPE/formation_a/-/tree/main/Arduino/AnimationLed_Button?ref_type=heads)

## DashBoard NodeRed

### Installation du module Dashboard

Cliquez sur l'icône du menu en haut à gauche, puis sélectionnez « Manage
Palette ».\
Ensuite, cliquez sur l'onglet « Install », tapez « node-red-dashboard »
dans le champ de recherche, puis sur « Install », pour démarrer
l'installation.\
Une fois l'installation terminée, dans la partie « Nodes »,vous avez
tout un tas de nouveaux nœuds qui ont été installés à la rubrique
Dashboard (switch, button...), ce sont principalement eux qui vont nous
servir à créer notre interface.

![](/Formation_Kurioscape_fichiers/image030.gif)

### Créer un premier Dashboard simple.

Avant d'ajouter nos actionneurs, il va nous falloir créer une page qui
va s'afficher sur le navigateur. Le minimum pour Node-Red, c'est de
créer un « Layout », une page de base, et un « Group », une zone dans
laquelle vont s'afficher nos objets.

Cliquez sur l'icône « Dashboard » en haut à droite.\
![](/Formation_Kurioscape_fichiers/image032.gif)

Dans l'onglet « Layout », cliquez sur « + Tabs », cela va ajouter une
page\
Cliquez ensuite sur « + Groups » pour ajouter un groupe, puis nommez-le.
Dans l'exemple « Actionneurs »,\
Pour voir, et commander quelque chose, il va falloir ajouter des boutons
d'interaction

### Ajouter un actionneur au Dashboard

L'actionneur le plus simple est le bouton.

Dans la liste des nœuds, cherchez le bouton « button » et ajouter le à
la page.

Reliez celui-ci a un MQTT OUT

![](/Formation_Kurioscape_fichiers/image034.gif)

Il faut ensuite paramétrer le bouton pour l'afficher dans le bon layout
créer précédemment.

Le bouton sera placer dans le group \[tab1\] layout : Actionneur

Lorsque l'on actionnera le bouton, il enverra le message suivant :

-   Payload : Scanner

-   Topic : server/command/animation

Ces paramètres permettre de passer les bonnes informations pour envoyer
un message MQTT et changer le mode Led sur la carte ESP 32 en mode
Scanner

![](/Formation_Kurioscape_fichiers/image035.png)

![](/Formation_Kurioscape_fichiers/image037.png)

Il est possible maintenant d'afficher l'interface en cliquant sur le
bouton en haut a droite de la fenetre de parametrage du module Dashboard

![](/Formation_Kurioscape_fichiers/image041.gif)

Pour arriver au résultat suivant :

![](/Formation_Kurioscape_fichiers/image043.gif)

En cliquant sur le bouton, vous devriez pouvoir changer le mode
d'affichage des Leds !
