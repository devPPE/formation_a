# La formation 
## Introduction aux technologie et outils
### Environnement de test
L'environnement de test se presentera sous la forme d'un mini projet permetant le pilotage d'un bandeau led et l'écoute de l'action sur un bouton brancher sur une **carte ESP32** compatible **arduino** avec des échanges MQTT pour communiquer avec un serveur **NodeRed**
<img src="Image/projet.jpg" width="400">
 
Pour cela nous allons présenter les élements suivant :
1. Le serveur NodeRed 
2. La carte ESP32 
3. L'environnement Arduino
4. Le protocol MQTT  

Une fois cela presenter, nous passerons à l'installation complete de l'environnement sont utilisation pour arrivé au résultat attendu. 

Les outils tournerons sur un Rapsberry. Celui-ci permettra d'heberger le serveur nodeRed mais aussi les service MQTT pour y connecter tout nos modules compatibles.

### Le serveur NodeRed

NodeRed est un service permettant de gérer un serveur avec une interface simplifier et "sans" code. Il permet grace à de simple noeuds et connection de programmer des actions.  

#### Interface

<img src="Image/NodeRed_A.png" width="800">

L’interface de Node-RED se compose de 4 parties, qui sont :

1. La liste des noeuds disponible  
Il sagit de la liste des noeuds disponible avec NodeRed pour l'edition de programme. Il est possible d'en rajouter en installant des plugin pour gerer differente tache (Accès à une base de données, Utilisation de map, Utilisation de service en ligne "alerte SMS", "alerte mail" ...)

<img src="Image/NR_B.png">

2. Les flows  
L'editeur de flow permet grace au noeud et a des lien entre eux de programmer les actions voulus.  
Il est possible de creer plusieur flow pour separer les differentes logiques (exemple : flow pour la gestion des scores, flow pour la gestion des salles ...)

<img src="Image/NR_C.png"  width="800">  

3. Fenetre d'information  
Cette fenetre regroupe plusieur onglet permettant d'obtenir des informations sur different aspects de l'environnement NodeRed  
    1. Debug  
    <img src="Image/NR_D.png" width="200">  
    2. Context  
    <img src="Image/NR_F.png" width="200">  
    3. Documentation  
    <img src="Image/NR_E.png" width="200">  

#### Noeuds de base
1. Common  
Noeuds communes, permettant des opérations simples sans traitements.  
<img src="Image/NR_bb.png"> 
Permet d’afficher un message de débug.  
<img src="Image/NR_aa.png"> 
Permet de lancer une action depuis Node Red pour activer les noeuds avec une information d'entré specifique  

2. Function  
Noeuds permettant d’agir sur les messages, de modifier leur contenu, de leur soumettre des traitements, et d’influer légèrement sur la façon dont ils sont délivrés  
<img src="Image/NoeudFunction.png">
Permet de créer une fonction en JavaScript. Utile pour traiter un message reçu pour le rendre utilisable par une fonction js
<img src="Image/NoeudDelay.png">
Permet de lancer une action depuis Node Red pour activer les noeuds avec une information d'entré specifique

3. MQTT  
Noeuds permettant la gestion des flux MQTT  
<img src="Image/mqttIn.png">
Permet de recevoir des message MQTT et des les utiliser dans des noeud par la suite  
<img src="Image/mqttOut.png">
Permet l'envois de messages MQTT depuis un flux node red

#### Edition de flows  
Pour creer un programe il faut :  
<img src="Image/NR_G.png">
1. Ajouter des noeuds
2. Les parametrer  
3. Les associer par des liaisons  
    Les noeuds associé échange des données grâce à une varibale **msg**. celle-ci est constituer par defaut d'un payload et d'un topic qui peuvent etre definis et recuperer par les noeud ou les utilisateur.    
    

### La carte ESP32  

La carte ESP32 est une carte programmable avec la librairie Arduino permettant d'executer du code.
Elle est capable de communiquer Wifi, Blutooth, Ethernet selon les modèles. Elle possédent des pin pouvant etre configurer en entré ou en sortie
* En entré  
    Elles permettent d'écouter des messages venant de differentes interface tel que des capteurs de température, de simple bouton ou encore la communication avec un lecteur RFID ...
* En sortie 
    Elles permettent de piloté différent élément allant de la simple Led au controle de module externe comme l'ouverture de porte, L'envois de SMS a partir d'un module GSM ... 

Elle a l'avantage d'etre compatible avec un grand nombre de protocole, de consommer tres peu et d'etre tres peu cher.  
<img src="Image/esp32.png" width="400">

### L'environnement Arduino

L'Arduino, combiné à la carte ESP32, offre une plateforme flexible pour les projets de développement embarqué et IoT (Internet des Objets). 

Le démarrage avec l'ESP32 sous l'environnement Arduino est simple. Après l'installation de l'IDE Arduino et l'ajout de l'ESP32 à la liste des cartes via le gestionnaire de cartes, Il est possible de programmer la carte avec toute les librairie accessible dans arduino

#### exemple de code 

>void setup() {  
>  // Configure la broche 13 en sortie  
>  pinMode(13, OUTPUT);  
>}
>
>void loop() {
>  digitalWrite(13, HIGH);   // Allume la LED  
>  delay(1000);               // Attends une seconde (1000 millisecondes)  
>  digitalWrite(13, LOW);    // Éteint la LED  
>  delay(1000);               // Attends encore une seconde  
>}

setup() : Cette fonction est appelée une seule fois lorsque le programme commence. Elle est utilisée pour initialiser les paramètres, comme la configuration des broches en entrée ou en sortie. Dans notre exemple, pinMode(13, OUTPUT); configure la broche numérique 13 comme une sortie. C'est nécessaire pour contrôler la LED.

loop() : Après setup(), la fonction loop() est appelée de manière répétitive aussi longtemps que l'appareil est sous tension. Cela permet au programme de changer d'état ou de réagir à des événements en continu. Dans l'exemple, nous utilisons loop() pour faire clignoter une LED :

* digitalWrite(13, HIGH); envoie une tension haute (5V ou 3.3V selon la carte) sur la broche 13, allumant ainsi la LED.  
* delay(1000); fait pause le programme pendant 1000 millisecondes (1 seconde), laissant la LED allumée pendant ce temps.  
* digitalWrite(13, LOW); envoie ensuite une tension basse (0V) sur la broche 13, éteignant la LED.  
* Un autre delay(1000); fait de nouveau pause le programme pendant 1 seconde, avec la LED éteinte cette fois.  
* La fonction loop() recommence ensuite, créant un effet de clignotement.  

#### Les librairie importante
* MQTT et WIFI  
Pour utiliser la bibliothèque PubSubClient avec MQTT sur un Arduino ou un ESP32, vous avez besoin d'un broker MQTT (comme Mosquitto rapsberry) pour relayer les messages entre les appareils. 

Prérequis :
- Installez la bibliothèque PubSubClient dans l'IDE Arduino (via le Gestionnaire de bibliothèques).
- Assurez-vous que votre ESP32 est connecté à Internet via Wi-Fi.
- Ayez l'adresse du serveur MQTT, le port (généralement 1883 pour MQTT)

Voici un exemple simple qui montre comment connecter un ESP32 à un broker MQTT et publier/envoyer des messages sur un topic.


```
#include <WiFi.h>
#include <PubSubClient.h>

// Paramètres WiFi
const char* ssid = "yourSSID";
const char* password = "yourPASSWORD";

// Paramètres du serveur MQTT
const char* mqtt_server = "BrokerAdress";
const int mqtt_port = 1883;
const char* mqtt_user = "yourMQTTusername"; // Laissez vide si non requis
const char* mqtt_password = "yourMQTTpassword"; // Laissez vide si non requis

WiFiClient espClient;
PubSubClient client(espClient);

void setup_wifi() {
  delay(10);
  // Connexion au réseau WiFi
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
  }

  // WiFi connecté
}

void callback(char* topic, byte* payload, unsigned int length) {
  // Gérer les messages entrants
}

void reconnect() {
  // Boucle jusqu'à ce que nous soyons reconnectés
  while (!client.connected()) {
    if (client.connect("ESP32Client", mqtt_user, mqtt_password)) {
      // Abonnement
      client.subscribe("yourTopic");
    } else {
      delay(5000);
    }
  }
}

void setup() {
  setup_wifi();
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
}

void loop() {
  if (!client.connected()) {
    reconnect();
  }
  client.loop();

  // Publie un message toutes les 5 secondes
  client.publish("yourTopic", "hello world");
  delay(5000);
}
```

- setup_wifi() : Cette fonction se connecte à votre réseau Wi-Fi.  
- callback() : Fonction appelée chaque fois qu'un message est reçu sur un des topics souscrits. Ici, vous pouvez ajouter le code pour traiter les messages.  
- reconnect() : Tente de se reconnecter au broker MQTT et de souscrire à un topic.  
- setup() : Initialise la connexion Wi-Fi et configure le client MQTT avec le serveur et le port MQTT, et définit la fonction de callback pour les messages entrants.  
- loop() : S'assure que le client est toujours connecté au broker MQTT et publie un message sur un topic spécifique toutes les 5 secondes.  

### Le protocol MQTT
#### Presentation
MQTT est un protocole d’envoi de données basé sur tcp/ip.

Il se divise en 3 types d’entités :  
  - les publishers, qui envoient des messages  
  - les listeners ou subscribers qui écoutent pour reçevoir des messages  
  - Le broker, qui est le serveur qui fait le lien entre les 2.  
    
Les publishers et listeners communiquent grâce à un topic auquel ils sont «abonnés» (subscribed).
#### Utilisation 
<img src="Image/mqttBroker.jpeg" width="600">

Plusieurs devices envoient leurs messages au broker, en passant par le publisher. La function topic rajoute le champs topic aux messages des devices.  
Le **MQTT OUT** s’abonne à ces topics et y envoie les messages.  
Le **MQTT IN** reçoit les messages de tous les topics, en étant abonnée au topic ‘#’, qui représente tous les topics possibles.  
Les noeuds mqtt out et mqtt in on les mêmes propriété, qui sont :  

* Server : Le serveur sur lequel se trouve le broker. Le port par défaut est 1883.  
* Topic : Le Topic auquel s’abonner.
* QoS : Quality of Service, determine la façon dont sont traités les messages  
<img src="Image/qos.png" width="600">
La noeud mqtt out possède également la property retain, qui determine si le broker doit
retenir le message même si aucun listener n’est abonné au bon topic.  

### Le Projet
Pour le projet toute les source sont trouvable sur un depot en ligne : [https://gitlab.com/devPPE/formation_a](https://gitlab.com/devPPE/formation_a)
#### Installation RPI
##### Creation de la carte SD
1. Préparation de la carte SD
  - télécharger l'utilitaire raspberry sur https://downloads.raspberrypi.org/imager/imager_latest.exe
  - installer et lancer l'application
    - Selectionner le model du raspberry
    - Selectionner l'OS RASPBERRY PI OS
    - Selectionner le stockage prealablement charger (carte micro SD)
  - Faire suivant

  - Il demande si l'on veux appliquer des configuration custom. Faire modifier les reglage
    - Dans Général 
      - definir un nom d'hôte unique pour pouvoir si connecter facilement plus tard en ssh
      - definir un nom d'utilisateur et un mot de passe
      - Définir le SSID et mot de passe du wifi 
    - Dans Services 
      - Activer le SSH 
  - Enregistrer

  ---
2.  Connection et préparation du raspberry
- Brancher le raspberry avec la carte flasher et connecter vous a celui-ci en ssh
- Ouvril un terminal

  >ssh [nomUser]@[hostname]

- Définisser l'utilisateur root avec le password

  >sudo passwd root  

- Passer en root

  >su - 

- installer ensuite node-red  
  >sudo apt install build-essential git curl  
  >bash <(curl -sL https://raw.githubusercontent.com/node-red/linux-installers/master/deb/update-nodejs-and-nodered)
  >sudo systemctl enable nodered.service

- Fixer l'adresse IP
  >sudo nano /etc/dhcpcd.conf  
  >interface eth0  
  >static ip_address=192.168.1.100/24  
  >static routers=192.168.1.1  
  >static domain_name_servers=192.168.1.1 8.8.8.8  
  > sudo reboot

- installer le broker MQTT mosquitto

  >sudo apt-get install mosquitto
  >sudo apt-get -y install mosquitto-clients  
  >sudo nano /etc/mosquitto/mosquitto.conf  
  >sudo systemctl enable mosquitto.service  

- ajouter a la fin du fichier (/etc/mosquitto/mosquitto.conf)  

  >port 1883   
  >listener 9001   
  >protocol websockets 
    
Le raspberry est maintenant pret à être utilisé 

---
#### Preparation NodeRed
Pour vous connecter a NodeRed
- Connecter vous au meme reseau que le raspberry 
- lancer un navigateur sur le lien [192.168.1.100:1880](192.168.1.100:1880)
##### Creation des noeuds pour recevoir et envoyer des information MQTT
##### Test des noeuds


#### Preparation ESP32
##### Librairies 
Librairie utiliser pour le projet
- NeoPixelBus
- PubSubClient
- Wifi
- Adafruit_NeoPixel

Pour les installer, aller dans le librairie manager de Arduino (a gauche de la fenetre) et cherchez les installer les librairie. Une fois installer vous pourrez les utiliser dans votre projet.
##### Analyse du code source
Le code source du projet est trouvable sur [https://gitlab.com/devPPE/formation_a/-/tree/main/Arduino/AnimationLed_Button](https://gitlab.com/devPPE/formation_a/-/tree/main/Arduino/AnimationLed_Button?ref_type=heads)
##### Flash des carte ESP32
##### Test communication

