//
// Created by paulp on 22/03/2021.
//

#ifndef BALANCE_CODE_BALANCECOLOR_H
#define BALANCE_CODE_BALANCECOLOR_H

#endif //BALANCE_CODE_BALANCECOLOR_H

#include "BandeLed.h"
#include <Adafruit_NeoPixel.h>
#define colorpower 0.5
const static uint32_t color_black = Adafruit_NeoPixel::Color(0, 0, 0);
const static uint32_t color_white = Adafruit_NeoPixel::Color(255*colorpower, 255*colorpower, 255*colorpower);
const static uint32_t color_red = Adafruit_NeoPixel::Color(255*colorpower, 0, 0);
const static uint32_t color_green = Adafruit_NeoPixel::Color(0, 255*colorpower, 0);
const static uint32_t color_blue = Adafruit_NeoPixel::Color(0, 0, 255*colorpower);
const static uint32_t color_violet = Adafruit_NeoPixel::Color(153*colorpower, 0, 255*colorpower);

const static uint32_t color_black50 = Adafruit_NeoPixel::Color(0, 0, 0);
const static uint32_t color_white50 = Adafruit_NeoPixel::Color(120, 120, 120);
const static uint32_t color_red50 = Adafruit_NeoPixel::Color(120, 0, 0);
const static uint32_t color_green50 = Adafruit_NeoPixel::Color(0, 120, 0);
const static uint32_t color_blue50 = Adafruit_NeoPixel::Color(0, 0, 120);
const static uint32_t color_violet50 = Adafruit_NeoPixel::Color(75, 0, 120);