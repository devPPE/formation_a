#include <Arduino.h>
#include <NeoPixelBus.h>
#include "BandeLed.h"
#include <WiFi.h>
#include <PubSubClient.h>

#define ARRAY_SIZE(x) sizeof(x)/sizeof(x[0])
#define colorSaturation 20


String esp_functions[] = {"RainbowCycle", "Scanner","FunLoop","Fade","TheaterChase","ColorWipe","WHITE","GREEN","RED","BLUE","VIOLET" };
// String esp_func = esp_functions[0];
int indexAnimation = 0;
int esp_func_length = ARRAY_SIZE(esp_functions);
BandeLed *l1; //Declaration d'un ruban Led


//Déclaration des informqtion Wifi et MQTT
WiFiClient espClient;
PubSubClient client(espClient);
const char* ssid = "boxscores_network1";
const char* password = "boxscores";
const char* mqtt_server = "192.168.4.155";
const char* client_id = "ESPTuto";
const char* willTopic = "server/ESPStatus";
const char* changeAnimationTopic = "ESP/LedStatus/changeAnimationLed";

// Declaration des information du bouton
const int buttonPin = 4; // Le numéro de la broche à laquelle le bouton est connecté
int buttonState;         // La dernière lecture du bouton
int lastButtonState = LOW; // L'état précédent du bouton
unsigned long lastDebounceTime = 0;  // Le dernier moment où l'output du bouton a basculé
unsigned long debounceDelay = 50;    // Le délai de rebond; augmentez si des rebonds sont toujours détectés


void setup_led();
void changeAnimationLed();
void setup_wifi();
void reconnect();
void callback(char* topic, byte * message, unsigned int length);
void sendAnimationChangeMessage(const String& animationName);

//Fonction permettant l'initialisation de la carte
void setup() {
    Serial.begin(115200); //définition de la vitesse du port serie pour la communication en serie
    setup_led(); // initialisation de la classe de gestion des led
    setup_wifi(); // Initialisation de la connection wifi
    client.setServer(mqtt_server, 1883); // initialisation de la connection mqtt
    client.setCallback(callback); //set up de la fontion de reception mqtt
    pinMode(4, INPUT_PULLUP); //configuration de la pin 4 de l'esp en INPUT_PULLUP
}

//Fonction permettant l'execution de nos actions
void loop() {

  int reading = digitalRead(buttonPin); // lecture de l'etat du bouton 4
  //
  //Le code suivant permet l'antirebond sur un bouton, cela evite les faux contact et la mauvaise prise en compte des appuis sur un bouton.
  if (reading != lastButtonState) {
    lastDebounceTime = millis();
  }
  if ((millis() - lastDebounceTime) > debounceDelay) {
      if (reading != buttonState) {
        buttonState = reading;
        if (buttonState == LOW) {
          Serial.println("Bouton pressé");
          Serial.println("je lance ChooseFunction");
          changeAnimationLed(); // si le bouton est actionné alors on appel la fonction changeAnimationLed() qui change l'animation en cours sur le ruban led
        }
      }
    }
  lastButtonState = reading;
  l1->Update(); //Cette ligne permet de jouer l'animation du ruban Led. A chaque fois que la loop recommence cette fonction est appelée et actualse l'etat des led pour creer un effet d'animation.
   if (!client.connected()) { //Si le client pert la connection on lance la fonction de reconnection.
    reconnect();
    }
  client.loop(); //fonction MQTT pour permettre le fonctionnement continue de l'ecoute et de l'envois MQTT
}

//Fonction d'initialisation de la connection wifi
void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

//Fonction de connection reconnection au service MQTT
void reconnect() {
  char bufSubscribe[50];

  String TopicSub = "";

  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect(client_id, willTopic, 1, true, "DISCONNECTED")) { //initilisation de la conncection mqtt avec un willTopic "server/ESPStatus" sur lequel sera renvoiyer l'etat DISCONNECTED ou CONNECTED
      Serial.println("connected");
      client.publish(willTopic, "CONNECTED");

      TopicSub = "server/command/#"; 
      TopicSub.toCharArray(bufSubscribe, TopicSub.length() + 1);
      client.subscribe(bufSubscribe); //inscription au topic (TopicSub = "server/command/#";) pour ecouter tout les message sur server/command/ 
    } else { //Si la connection echou alors on renouvelle la tentative de connection.
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

//fonction d'ecoute mqtt permettant la recuperation des message mqtt sur les topic auxquelles nous avons souscris precedement
void callback(char* topic, byte * message, unsigned int length) {
  Serial.print("Message arrived on topic: ");
  Serial.println(topic);

  String messageTemp;
  String Topic = "";
  int NumTrav = 0;
  Topic = String(topic);
  for (int i = 0; i < length; i++) {
    messageTemp += (char)message[i];
  }
  Serial.print(". Message: ");
  Serial.println(messageTemp);
  
  //modification animation par parametre dans message server/command
  // if (Topic.indexOf("command") > 0) {
    if (Topic == "server/command/animation") {
      if(messageTemp == "Scanner"){
        l1->Scanner(l1->Color(255, 0, 255), 100);
      }else if(messageTemp == "Rainbow"){
        l1->RainbowCycle(10);
      }
      l1->Update();
  }else if(Topic == "server/command/fixe"){
      if(messageTemp == "GREEN"){
        l1->FixColor(color_green50);
      }
      if(messageTemp == "RED"){
        l1->FixColor(color_red50);
      }
      else if(messageTemp == "BLUE"){
        l1->FixColor(color_blue50);
      }
      else if(messageTemp == "VIOLET"){
        l1->FixColor(color_violet50);
      }
      else if(messageTemp == "WHITE"){
        l1->FixColor(color_white50);
      }
      l1->Update();
  }

}

//Fonction d'envois MQTT pour prevenir sur le changeAnimationTopic ESP/LedStatus/changeAnimationLed du changement d'etat d'animation du ruban.
void sendAnimationChangeMessage(const String& animationName) {
    if(client.connected()) {
        client.publish(changeAnimationTopic, animationName.c_str());
    } else {
        Serial.println("MQTT not connected, cannot send animation change message.");
    }
}

//Permet d'initialiser les rubans Led
void setup_led() {

    Serial.println("init led");
    l1 = new BandeLed(10, 19, NEO_GRB + NEO_KHZ800); //initialisation du ruban led avec 10 leds et piloté par le port 19 de la carte
    l1->begin();
    l1->RainbowCycle(10);
    l1->Update();
}

//Permet de changer l'animation en court sur le ruban led l1, la fonction n'a pas de parametre permettant de choisir le ruban led a piloter
void changeAnimationLed(){
    Serial.println("changeAnimationLed");
    String selectAnim = esp_functions[indexAnimation];
    Serial.print("animation select : ");
    Serial.println(selectAnim);
    if (selectAnim == "RainbowCycle") {
        l1->RainbowCycle(10);
    }
    if (selectAnim == "Scanner") {
        l1->Scanner(l1->Color(255, 0, 255), 100);
    }
    if (selectAnim == "FixColor") {
        l1->FixColor(color_green50);
    }
    if (selectAnim == "GREEN") {
        l1->FixColor(color_green50);
    }
    if (selectAnim == "RED") {
        l1->FixColor(color_red50);
    }
    if (selectAnim == "BLUE") {
        l1->FixColor(color_blue50);
    }
    if (selectAnim == "VIOLET") {
        l1->FixColor(color_violet50);
    }
    if (selectAnim == "WHITE") {
        l1->FixColor(color_white50);
    }
    if (selectAnim == "FunLoop") {
        l1->FunLoop(l1->Color(15, 255, 255), l1->Color(0, 255, 0), 100);
    }
    if (selectAnim == "Fade") {
        l1->Fade(l1->Color(255, 255, 0), l1->Color(0, 0, 50), 100, 20);
    }
    if (selectAnim == "TheaterChase") {
        l1->TheaterChase(l1->Color(255, 255, 0), l1->Color(0, 0, 50), 100);
    }
    if (selectAnim == "ColorWipe") {
        l1->ColorWipe(l1->Color(255, 255, 0), 10);
    }
    l1->Update();
//    delay(500);
  sendAnimationChangeMessage(esp_functions[indexAnimation]);
    if (indexAnimation < esp_func_length-1){
        indexAnimation += 1;
    }else
    {
        indexAnimation = 0;
    }
};