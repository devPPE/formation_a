//
// Created by paulp on 17/03/2021.
//

#include "BandeLed.h"

// Update the pattern
void BandeLed::Update() {
//    Serial.print("ActivePattern:");
//    Serial.println(ActivePattern);
    if ((millis() - lastUpdate) > Interval) // time to update
    {
        lastUpdate = millis();
        switch (ActivePattern) {

            case FIXCOLOR:
                FixColorUpdate();
                break;
            case RAINBOW_CYCLE:
                RainbowCycleUpdate();
                break;
            case THEATER_CHASE:
                TheaterChaseUpdate();
                break;
            case COLOR_WIPE:
                ColorWipeUpdate();
                break;
            case SCANNER:
                ScannerUpdate();
                break;
            case FADE:
                FadeUpdate();
                break;
            case FUNLOOP:
                FunLoopUpdate();
                break;
            case TIMER:
                TimerUpdate();
                break;
            default:
                break;
        }
    }
}

// Increment the Index and reset at the end
void BandeLed::Increment() {
//    Serial.print("ActivePattern:");
//    Serial.print(Index);
//    Serial.print("::");
//    Serial.println(TotalSteps);
    if (Direction == FORWARD) {
        Index++;
        if (Index >= TotalSteps) {
            Index = 0;
            if (OnComplete != NULL) {
                OnComplete(); // call the comlpetion callback
            }
        }
    } else // Direction == REVERSE
    {
        --Index;
        if (Index <= 0) {
            Index = TotalSteps - 1;
            if (OnComplete != NULL) {
                OnComplete(); // call the completion callback
            }
        }
    }
}

// Reverse pattern direction
void BandeLed::Reverse() {
    if (Direction == FORWARD) {
        Direction = REVERSE;
        Index = TotalSteps - 1;
    } else {
        Direction = FORWARD;
        Index = 0;
    }
}

void BandeLed::FixColor(uint32_t color1,uint8_t interval, direction dir){
    ActivePattern = FIXCOLOR;
    Interval = interval;
    TotalSteps = 255;
    Index = 0;
    Direction = dir;
    Color1 = color1;
}
void BandeLed::FixColorUpdate()
{
    ColorSet(Color1);
    show();
    Increment();
}
// Initialize for a RainbowCycle
void BandeLed::RainbowCycle(uint8_t interval, direction dir) {
    ActivePattern = RAINBOW_CYCLE;
    Interval = interval;
    TotalSteps = 255;
    Index = 0;
    Direction = dir;
}

// Update the Rainbow Cycle Pattern
void BandeLed::RainbowCycleUpdate() {
    for (int i = 0; i < numPixels(); i++) {
        setPixelColor(i, Wheel(((i * 256 / numPixels()) + Index) & 255));
    }
    show();
    Increment();
}

// Initialize for a Theater Chase
void BandeLed::TheaterChase(uint32_t color1, uint32_t color2, uint8_t interval, direction dir) {
    ActivePattern = THEATER_CHASE;
    Interval = interval;
    TotalSteps = numPixels();
    Color1 = color1;
    Color2 = color2;
    Index = 0;
    Direction = dir;
}

// Update the Theater Chase Pattern
void BandeLed::TheaterChaseUpdate() {
    for (int i = 0; i < numPixels(); i++) {
        if ((i + Index) % 3 == 0) {
            setPixelColor(i, Color1);
        } else {
            setPixelColor(i, Color2);
        }
    }
    show();
    Increment();
}

void BandeLed::FunLoop(uint32_t color1, uint32_t color2, uint8_t interval, direction dir) {
    ActivePattern = FUNLOOP;
    Interval = interval;
    TotalSteps = numPixels();
    Color1 = color1;
    Color2 = color2;
    Index = 0;
    Direction = dir;

}

void BandeLed::FunLoopUpdate() {

    for (int i = 0; i < numPixels(); i++) {
        int nb = numPixels();

        int pos1 = Index % nb;
        int pos2 = (Index + (nb / 4)) % nb;
        int pos3 = (Index + (nb / 2)) % nb;
        int pos4 = (Index + (3 * (nb / 4))) % nb;

        if (i == pos1) {
            setPixelColor(i - 1, Color1);
            setPixelColor(i, Color1);
            setPixelColor(i + 1, Color1);
        }
        if (i == pos2) {
            setPixelColor(i - 1, Color1);
            setPixelColor(i, Color1);
            setPixelColor(i + 1, Color1);
        }
        if (i == pos3) {
            setPixelColor(i - 1, Color1);
            setPixelColor(i, Color1);
            setPixelColor(i + 1, Color1);
        }
        if (i == pos4) {
            setPixelColor(i - 1, Color1);
            setPixelColor(i, Color1);
            setPixelColor(i + 1, Color1);
        } else {
            setPixelColor(i, Color2);
        }
    }
    show();
    Increment();
}

// Initialize for a ColorWipe
void BandeLed::ColorWipe(uint32_t color, uint8_t interval, direction dir) {
    ActivePattern = COLOR_WIPE;
    Interval = interval;
    TotalSteps = numPixels();
    Color1 = color;
    Index = 0;
    Direction = dir;
}

// Update the Color Wipe Pattern
void BandeLed::ColorWipeUpdate() {
    setPixelColor(Index, Color1);
    show();
    Increment();
}

// Initialize for a SCANNNER
void BandeLed::Scanner(uint32_t color1, uint8_t interval) {
    ActivePattern = SCANNER;
    Interval = interval;
    TotalSteps = (numPixels()) * 2;
    Color1 = color1;
    Index = 0;
    Direction = FORWARD;
}

// Update the Scanner Pattern
void BandeLed::ScannerUpdate() {
    for (int i = 0; i < numPixels(); i++) {

        if (i == Index)  // Scan Pixel to the right
        {
            setPixelColor(i, Color1);
        } else if (i == TotalSteps - Index) // Scan Pixel to the left
        {
            setPixelColor(i, Color1);
        } else // Fading tail
        {
            setPixelColor(i, DimColor(getPixelColor(i)));
        }
    }
    show();
    Increment();
}

// Initialize for a Fade
void BandeLed::Fade(uint32_t color1, uint32_t color2, uint16_t steps, uint8_t interval, direction dir) {
    ActivePattern = FADE;
    Interval = interval;
    TotalSteps = steps;
    Color1 = color1;
    Color2 = color2;
    Index = 0;
    Direction = dir;
}

// Update the Fade Pattern
void BandeLed::FadeUpdate() {
    // Calculate linear interpolation between Color1 and Color2
    // Optimise order of operations to minimize truncation error
    uint8_t red = ((Red(Color1) * (TotalSteps - Index)) + (Red(Color2) * Index)) / TotalSteps;
    uint8_t green = ((Green(Color1) * (TotalSteps - Index)) + (Green(Color2) * Index)) / TotalSteps;
    uint8_t blue = ((Blue(Color1) * (TotalSteps - Index)) + (Blue(Color2) * Index)) / TotalSteps;

    ColorSet(Color(red, green, blue));
    show();
    Increment();
}

// Calculate 50% dimmed version of a color (used by ScannerUpdate)
uint32_t BandeLed::DimColor(uint32_t color) {
    // Shift R, G and B components one bit to the right
    uint32_t dimColor = Color(Red(color) >> 1, Green(color) >> 1, Blue(color) >> 1);
    return dimColor;
}

// Set all pixels to a color (synchronously)
void BandeLed::ColorSet(uint32_t color) {
    for (int i = 0; i < numPixels(); i++) {
        setPixelColor(i, color);
    }
    show();
}

// Returns the Red component of a 32-bit color
uint8_t BandeLed::Red(uint32_t color) {
    return (color >> 16) & 0xFF;
}

// Returns the Green component of a 32-bit color
uint8_t BandeLed::Green(uint32_t color) {
    return (color >> 8) & 0xFF;
}

// Returns the Blue component of a 32-bit color
uint8_t BandeLed::Blue(uint32_t color) {
    return color & 0xFF;
}

// Input a value 0 to 255 to get a color value.
// The colours are a transition r - g - b - back to r.
uint32_t BandeLed::Wheel(byte WheelPos) {
    WheelPos = 255 - WheelPos;
    if (WheelPos < 85) {
        return Color(255 - WheelPos * 3, 0, WheelPos * 3);
    } else if (WheelPos < 170) {
        WheelPos -= 85;
        return Color(0, WheelPos * 3, 255 - WheelPos * 3);
    } else {
        WheelPos -= 170;
        return Color(WheelPos * 3, 255 - WheelPos * 3, 0);
    }
}

void BandeLed::Timer(uint32_t color1, uint8_t interval, direction dir) {
    ActivePattern = TIMER;
    Interval = interval;
    Color1 = color1;
    Index = 0;
    Direction = dir;

}

void BandeLed::TimerUpdate() {
    this->fill(color_black);
    for (int i = 0; i < numPixels(); i++) {
        if (i <= Index) {
            setPixelColor(i, Color1);
        }
    }
    Increment();
    show();
}















